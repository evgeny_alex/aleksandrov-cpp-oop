#pragma once
#include <iostream>
#include "for_each.h"

struct callback
{
	template<typename T>
	void operator()(int index, T&& t) // index - ��� ������� �������� � �������
	{                                 // t - �������� ��������
		std::cout << "#" << index << " = " << t << " ";
	}

};

template<typename... Args>
auto operator<<(std::ostream& os, std::tuple<Args...> const& t) {
	for_each(t, callback());
}