#include "pch.h"
#include "../Task_2_v2/calculator.cpp"
#include "../Task_2_v2/operation.cpp"

TEST(Test_Command, plus) {

	Stack_Calc stack_calc;
	ifstream file("test_plus.txt");
	stack_calc.work_with_file(file);
	EXPECT_EQ(6, stack_calc.get_top_stack());
}

TEST(Test_Command, minus) {
	Stack_Calc stack_calc;
	ifstream file("test_minus.txt");
	stack_calc.work_with_file(file);
	EXPECT_EQ(4, stack_calc.get_top_stack());
}

TEST(Test_Command, push) {
	Stack_Calc stack_calc;
	ifstream file("test_push.txt");
	stack_calc.work_with_file(file);
	EXPECT_EQ(1000.01, stack_calc.get_top_stack());
}

TEST(Test_Command, pop) {
	Stack_Calc stack_calc;
	ifstream file("test_pop.txt");
	stack_calc.work_with_file(file);
	EXPECT_EQ(1, stack_calc.get_top_stack());
}

TEST(Test_Command, mul) {
	Stack_Calc stack_calc;
	ifstream file("test_mul.txt");
	stack_calc.work_with_file(file);
	EXPECT_EQ(125, stack_calc.get_top_stack());
}

TEST(Test_Command, div) {
	Stack_Calc stack_calc;
	ifstream file("test_div.txt");
	stack_calc.work_with_file(file);
	EXPECT_EQ(5, stack_calc.get_top_stack());
}

TEST(Test_Command, define) {
	Stack_Calc stack_calc;
	ifstream file("test_define.txt");
	stack_calc.work_with_file(file);
	EXPECT_EQ(5, stack_calc.get_top_stack());
}

TEST(Test_Command, sqrt) {
	Stack_Calc stack_calc;
	ifstream file("test_sqrt.txt");
	stack_calc.work_with_file(file);
	EXPECT_EQ(4, stack_calc.get_top_stack());
}

TEST(Test_Exception, value) {
	Stack_Calc stack_calc;
	ifstream file("test_exc_val.txt");
	while (true) {
		try {
			EXPECT_THROW(stack_calc.work_with_file(file), ValException);
			return;
		}
		catch (Exception& e) {
			cout << e.what() << endl;
		}
	}
	
}

TEST(Test_Exception, command) {
	Stack_Calc stack_calc;
	ifstream file("test_exc_com.txt");
	while (true) {
		try {
			EXPECT_THROW(stack_calc.work_with_file(file), CommandException);
			return;
		}
		catch (Exception& e) {
			cout << e.what() << endl;
		}
	}
}

TEST(Test_Exception, push) {
	Stack_Calc stack_calc;
	ifstream file("test_exc_push.txt");
	while (true) {
		try {
			EXPECT_THROW(stack_calc.work_with_file(file), PushStackException);
			return;
		}
		catch (Exception& e) {
			cout << e.what() << endl;
		}
	}
}

TEST(Test_Exception, pop) {
	Stack_Calc stack_calc;
	ifstream file("test_exc_pop.txt");
	while (true) {
		try {
			EXPECT_THROW(stack_calc.work_with_file(file), PopStackException);
			return;
		}
		catch (Exception& e) {
			cout << e.what() << endl;
		}
	}
}

TEST(Test_Exception, arg) {
	Stack_Calc stack_calc;
	ifstream file("test_exc_arg.txt");
	while (true) {
		try {
			EXPECT_THROW(stack_calc.work_with_file(file), ArgStackException);
			return;
		}
		catch (Exception& e) {
			cout << e.what() << endl;
		}
	}
}

TEST(Test_Exception, print) {
	Stack_Calc stack_calc;
	ifstream file("test_exc_print.txt");
	while (true) {
		try {
			EXPECT_THROW(stack_calc.work_with_file(file), PrintStackException);
			return;
		}
		catch (Exception& e) {
			cout << e.what() << endl;
		}
	}
}

TEST(Test_Exception, div_by_zero) {
	Stack_Calc stack_calc;
	ifstream file("test_exc_div_by_zero.txt");
	while (true) {
		try {
			EXPECT_THROW(stack_calc.work_with_file(file), Div_by_zero_Exception);
			return;
		}
		catch (Exception& e) {
			cout << e.what() << endl;
		}
	}
}

TEST(Test_Exception, neg_root) {
	Stack_Calc stack_calc;
	ifstream file("test_exc_neg_root.txt");
	while (true) {
		try {
			EXPECT_THROW(stack_calc.work_with_file(file), Neg_root_Exception);
			return;
		}
		catch (Exception& e) {
			cout << e.what() << endl;
		}
	}
}