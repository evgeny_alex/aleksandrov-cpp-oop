#pragma once
#include "player.h"

class PlayerBase {
private:
	bool defeat_flag;				//	���� ���������, �������� �� �����
	short map_ship[N][N];			//	���� ��� ��������
	short map_strike[N][N];			//	���� ��� ������
	vector<Ship> set_of_ships;		//	����, � ������� �������� �������
	int count_of_ships;
	int scores;						// ���������� ������ ��������
	int points;						// ���������� ���������� �������
	GameView* gameview;

	
	bool is_this_ship(Ship &, short, short);	// �������� � ���� �� �������
public:
	PlayerBase();
	void print_map_ship(string);
	void erase_data();
	int get_points();
	void add_points();
	void set_ship_on_map_strike(Ship);
	void print_map_strike(string);
	Ship get_ship(int, int);
	void add_scores();
	void lost();
	int get_scores();
	bool get_flag();
	int get_count_ships();
	short get_val_map_ship(int, int);
	short get_val_map_strike(int, int);
	void set_val_map_strike(int, int, short);
	bool impact_on_ships(short, short);
	bool check_ship(short, short);				// ���������� 0, ���� ������, 1, ���� ��������� 
	
	void set_one_ship(int, int, Direction, count_of_deck, shared_ptr<Player>);
	void set_ship_on_map(Ship);
};