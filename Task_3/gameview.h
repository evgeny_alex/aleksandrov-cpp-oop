#pragma once
#include <string>
#include <map>

using namespace std;

const int M = 10;

class GameView {
public:
	virtual void print_board_of_ship(short map[M][M], string) = 0;
	virtual void print_board_of_strike(short map[M][M], string) = 0;
	virtual void print_winner_of_round(string, int, int, int) = 0;
	virtual void print_statistic(map<int, string>&) = 0;
	virtual void print_current_round(int) = 0;
	virtual void print_winner(string, string, int, int) = 0;
	virtual void print_enter_name(int) = 0;
	virtual void print_enter_coordinates(int, string) = 0;
	virtual void clear_screen() = 0;
	virtual void print_scores(string, string, int, int) = 0;
	virtual void print_destroy_ship() = 0;
	virtual void print_deck_shot_down() = 0;
	virtual void print_exception(const char *) = 0;
	virtual void print_coord_for_strike(string) = 0;
	virtual void print_didnt_hit() = 0;
};

class ConsoleView : public GameView {
	public:
		ConsoleView() {}
		void print_board_of_ship(short map[M][M], string);
		void print_board_of_strike(short map[M][M], string);
		void print_winner_of_round(string, int, int, int);
		void print_statistic(map<int, string>&);
		void print_current_round(int);
		void print_winner(string, string, int, int);
		void print_enter_name(int);
		void print_enter_coordinates(int, string);
		void clear_screen();
		void print_scores(string, string, int, int);
		void print_destroy_ship();
		void print_deck_shot_down();
		void print_exception(const char *);
		void print_coord_for_strike(string);
		void print_didnt_hit();
};