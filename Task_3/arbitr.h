#pragma once
#include "player.h"
#include "playerbase.h"

enum CurrentPlayer {first, second};

class Arbitr {
private:
	GameView* gameview;
	void check_location(int, int, Direction, count_of_deck);
	void check_coord_strike(int, int);
	vector<shared_ptr<PlayerBase>> player_base;
	CurrentPlayer current_player;

public:
	Arbitr();
	void print_map_ship(int, string);
	void erase_data();
	int get_points(int);
	void add_points(int);
	int get_scores(int);
	bool get_flag_player(int);
	void print_map_strike(string, int);
	CurrentPlayer get_current_player();
	void set_current_player(CurrentPlayer);
	void set_ship(count_of_deck, shared_ptr<Player>);
	void placement_ship(shared_ptr<Player>);
	bool check_move(shared_ptr<Player>);
};