#include "player.h"
#include <cassert>

/* ������ ����������� ������. */

ConsolePlayer::ConsolePlayer() { // ����������� ������ ConsolePlayer
	flag_view = true;
	gameview = new ConsoleView();
}

void ConsolePlayer::enter_name() { // ���� �����
	getline(cin, name);
}


pair<int, int> ConsolePlayer::generate_coord(count_of_deck count_of_deck, int current_ship) {
	pair<int, int> pair;
	string str;
	//gameview->print_board_of_ship(map_ship, name);	// �������� ����� �������������
	gameview->print_enter_coordinates(count_of_deck, name);
	cin >> str; // ������ ����������
	

	if (str.size() > 2) throw StrException();
	pair.first = str[0] - 'A';
	pair.second = str[1] - '0';

	return pair;
}

int ConsolePlayer::generate_direction() {
	int d;
	cin >> d;
	gameview->clear_screen();
	return d;
}

pair<int, int> ConsolePlayer::generate_strike() {
	pair<int, int> pair;
	string str;
	cin >> str;			// ��������
	if (str.size() > 2) throw StrException();
	pair.first = str[0] - 'A';	// ��������� ��������� ����� �������
	pair.second = str[1] - '0';

	return pair;
}

void ConsolePlayer::destroy_ship(int X, int Y) {
	gameview->print_destroy_ship();
}

void ConsolePlayer::deck_shot_down(int X, int Y) {
	gameview->print_deck_shot_down();
}

/* ������ ���������� ������. */

RandomPlayer::RandomPlayer() { // ����������� ������ RandomPlayer
	flag_view = false;
	gameview = new ConsoleView();
}

pair<int, int> RandomPlayer::generate_coord(count_of_deck count_of_deck, int current_ship) {
	pair<int, int> pair;
	
	pair.first = rand() % 10;
	pair.second = rand() % 10;

	return pair;
}

int RandomPlayer::generate_direction() {
	int d;
	d = rand() % 2;
	gameview->clear_screen();
	return d;
}

pair<int, int> RandomPlayer::generate_strike() {
	pair<int, int> pair;
	
	pair.first = rand() % 10;
	pair.second = rand() % 10;

	return pair;
}

void RandomPlayer::enter_name() {
	name = "RandomPlayer";
}

/* ������ ������������ ������. */

OptimalPlayer::OptimalPlayer() : current_move(0), count_found_deck(0), state(find_deck) { // ����������� ������ RandomPlayer
	flag_view = false;
	gameview = new ConsoleView;
	dir_of_ships = v;
	i = 0;
	j = 0;

	count_found_4_deck_ship = 0;
	count_found_3_deck_ship = 0;
	count_found_2_deck_ship = 0;
	
	init_strategy();	// ������������� ��������� ������ 2, 3, 4-� �������� ��������
	init_big_ships();
}

pair<int, int> OptimalPlayer::generate_coord(count_of_deck, int current_ship) {
	pair<int, int> pair;

	if (current_ship < 6) {
		pair.first = x[current_ship];
		pair.second = y[current_ship];
	}
	else {
		pair.first = rand() % 10;
		pair.second = rand() % 10;
	}
	return pair;
}

int OptimalPlayer::generate_direction() {
	return dir_of_ships;
}

pair<int, int> OptimalPlayer::generate_strike() {
	pair<int, int> pair;
	switch (state) {
	case find_deck:
		if (count_found_4_deck_ship < four_deck) {
			pair.first = X_4_deck[current_move];
			pair.second = Y_4_deck[current_move];
			current_move++;
			return pair;
		}
		if (count_found_3_deck_ship < three_deck) {
			pair.first = X_3_deck[current_move];
			pair.second = Y_3_deck[current_move];
			current_move++;
			return pair;
		}
		if (count_found_2_deck_ship < two_deck) {
			pair.first = X_2_deck[current_move];
			pair.second = Y_2_deck[current_move];
			current_move++;
			return pair;
		}
		else {
			pair.first = i % 10;	// ��������� ��������� ����� �������
			pair.second = j % 10;
			j++;
			if (j % 10 == 0) i++;
			return pair;
		}

		break;
	case find_next_deck:
		pair.first = X_next_moves.back();
		X_next_moves.pop_back();
		pair.second = Y_next_moves.back();
		Y_next_moves.pop_back();
		return pair;
		break;
	case finish_ship:
		pair.first = X_next_moves.back();
		X_next_moves.pop_back();
		pair.second = Y_next_moves.back();
		Y_next_moves.pop_back();
		return pair;
	}

	return pair;
}

void OptimalPlayer::clear_next_moves(){
	X_next_moves.clear();
	Y_next_moves.clear();
}

void OptimalPlayer::destroy_ship(int X, int Y) {
	X_ship.push_back(X);
	Y_ship.push_back(Y);
	count_found_deck++;
	switch (count_found_deck) {
	case four:
		count_found_4_deck_ship++;
		if (count_found_4_deck_ship == four_deck) current_move = 0;
		break;
	case three:
		count_found_3_deck_ship++;
		if (count_found_3_deck_ship == three_deck) current_move = 0;
		break;
	case two:
		count_found_2_deck_ship++;
		if (count_found_2_deck_ship == two_deck) current_move = 0;
		break;
	}
	//ship_process_on_the_map(); ������ player_base
	X_ship.clear();
	Y_ship.clear();
	
	state = find_deck;
	count_found_deck = 0;
}

void OptimalPlayer::new_strategy() {
	count_found_4_deck_ship = 0;
	count_found_3_deck_ship = 0;
	count_found_2_deck_ship = 0;
	current_move = 0;
}

void OptimalPlayer::deck_shot_down(int X, int Y) {
	clear_next_moves();
	X_ship.push_back(X);
	Y_ship.push_back(Y);
	if (!count_found_deck) {	// ������� � ���������� ���������
		state = find_next_deck;
		X_next_moves.push_back(X);
		X_next_moves.push_back(X - 1);
		X_next_moves.push_back(X);
		X_next_moves.push_back(X + 1);

		Y_next_moves.push_back(Y - 1);
		Y_next_moves.push_back(Y);
		Y_next_moves.push_back(Y + 1);
		Y_next_moves.push_back(Y);
	}
	else {
		state = finish_ship;
		if (Y_ship.front() == Y_ship.back()) {
			X_next_moves.push_back(min(X_ship) - 1);
			X_next_moves.push_back(max(X_ship) + 1);
			Y_next_moves.push_back(Y);
			Y_next_moves.push_back(Y);
		}
		if (X_ship.front() == X_ship.back()) {
			Y_next_moves.push_back(min(Y_ship) - 1);
			Y_next_moves.push_back(max(Y_ship) + 1);
			X_next_moves.push_back(X);
			X_next_moves.push_back(X);
		}
	}
	count_found_deck++;
}

void OptimalPlayer::enter_name() {
	name = "OptimalPlayer";
}

void OptimalPlayer::init_strategy() {
	init_4_deck_ship();
	init_3_deck_ship();
	init_2_deck_ship();
}

void OptimalPlayer::init_4_deck_ship() {
	for (int j = 0; j < N; j++) {
		for (int i = 0; i < N; i++) {
			if (!((i + j + 1) % 4)) { // ������ ��������� ������ ���������
				X_4_deck.push_back(i);
				Y_4_deck.push_back(j);
			}
		}
	}
}

void OptimalPlayer::init_3_deck_ship() {
	for (int j = 0; j < N; j++) {
		for (int i = 0; i < N; i++) {
			if (!((i + j + 1) % 3)) {
				X_3_deck.push_back(i);
				Y_3_deck.push_back(j);
			}
		}
	}
}

void OptimalPlayer::init_2_deck_ship() {
	for (int j = 0; j < N; j++) {
		for (int i = 0; i < N; i++) {
			if (!((i + j + 1) % 2)) {
				X_2_deck.push_back(i);
				Y_2_deck.push_back(j);
			}
		}
	}
}


short OptimalPlayer::min(vector<short> &a){
	short min = N;
	for (size_t i = 0; i < a.size(); i++) {
		if (a[i] < min) min = a[i];
	}
	return min;
}

short OptimalPlayer::max(vector<short> &a) {
	short max = 0;
	for (size_t i = 0; i < a.size(); i++) {
		if (a[i] > max) max = a[i];
	}
	return max;
}

void OptimalPlayer::init_coordinates(vector<short> &x, vector<short> &y) {
	x.push_back(0); // ��������� ����������� ���������
	x.push_back(0);
	x.push_back(7);
	x.push_back(4);
	x.push_back(5);
	x.push_back(8);
	int i;
	y.push_back(0);
	for (i = 0; i < 2; i++)
		y.push_back(9);
	for (; i < 5; i++)
		y.push_back(0);

}

void OptimalPlayer::swap(short *a, short *b) {
	short tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
}

void OptimalPlayer::init_big_ships() {
	// ��������� �� ����������� ��������� 2, 3, 4-� ����������
	x.push_back(0); // ��������� ����������� ���������
	x.push_back(0);
	x.push_back(7);
	x.push_back(4);
	x.push_back(5);
	x.push_back(8);
	int i;
	y.push_back(0);
	for (i = 0; i < 3; i++)
		y.push_back(9);
	for (; i < 5; i++)
		y.push_back(0);
	srand((unsigned int)time(0)); // ������� ��� ������ �������� �������
	if (rand() % 2) {
		dir_of_ships = h;
		for (int j = 0; j < 6; j++)
			swap(&x[j], &y[j]);
	}
}

/* ������ ������ Player. */
bool Player::get_flag_view() {
	return flag_view;
}

string Player::get_name() {
	return name;
}