#include "ship.h"

Ship::Ship(short X, short Y, Direction direction, count_of_deck count_deck): X(X), Y(Y), direction(direction),
																			count_deck(count_deck), current_count_deck(count_deck) {}

short Ship::getX() {
	return X;
}

short Ship::getY() {
	return Y;
}

Direction Ship::getDir() {
	return direction;
}

count_of_deck Ship::getDeck() {
	return count_deck;
}

void Ship::reduce_deck() {
	current_count_deck--;
}

int Ship::getCurDeck() { // ���������� ������� ���������� �����
	return current_count_deck;
}
