#include "arbitr.h"

void Arbitr::check_location(int X, int Y, Direction d, count_of_deck c) {
	int dX = X, dY = Y;
	if ((X > 9) || (X < 0) || (Y > 9) || (Y < 0))
		throw CoordinatesException();
	if (d == h) dY = Y + c - 1;
	else dX = X + c - 1;
	if ((dX > 9) || (dY > 9))
		throw CoordinatesException();

	for (int i = X; i <= dX; i++) {
		for (int j = Y; j <= dY; j++) {
			if (player_base[current_player]->get_val_map_ship(i, j) == 1 || player_base[current_player]->get_val_map_ship(i, j) == 2)
				throw CoordinatesException();
		}
	}
}

void Arbitr::check_coord_strike(int X, int Y) {
	if ((X > 9) || (X < 0) || (Y > 9) || (Y < 0)) throw CoordinatesException();
}

Arbitr::Arbitr() : gameview(new ConsoleView()) {
	current_player = first;
	player_base.push_back(make_shared<PlayerBase>());
	player_base.push_back(make_shared<PlayerBase>());
}

void Arbitr::print_map_ship(int c, string name) {
	player_base[c]->print_map_ship(name);
}

void Arbitr::erase_data() {
	player_base[first]->erase_data();
	player_base[second]->erase_data();
}

int Arbitr::get_points(int c){
	return player_base[c]->get_points();
}

void Arbitr::add_points(int c) {
	player_base[c]->add_points();
}

int Arbitr::get_scores(int c) {
	return player_base[c]->get_scores();
}

bool Arbitr::get_flag_player(int c) {
	return player_base[c]->get_flag();
}

void Arbitr::print_map_strike(string name, int c) {
	player_base[c]->print_map_strike(name);
}

CurrentPlayer Arbitr::get_current_player() {
	return current_player;
}

void Arbitr::set_current_player(CurrentPlayer c) {
	current_player = c;
}

void Arbitr::set_ship(count_of_deck c, shared_ptr<Player> player) {
	pair<int, int> pair;
	Direction d = h;
	int dir;
	print_map_ship(current_player, player->get_name());
	pair = player->generate_coord(c, player_base[current_player]->get_count_ships());
	if (c == 1) dir = 1;
	else dir = player->generate_direction();
	if (dir != 0 && dir != 1) throw DirException();
	if (dir) d = v;
	else d = h;
	check_location(pair.first, pair.second, d, c);
	player_base[current_player]->set_one_ship(pair.first, pair.second, d, c, player);
}

void Arbitr::placement_ship(shared_ptr<Player> player) {
	int i1 = 0, i2 = 0, i3 = 0, i4 = 0;
	while (true) {
		try {
			for (; i1 < four_deck; i1++)
				set_ship(four, player);
				
			for (; i2 < three_deck; i2++)
				set_ship(three, player);
					
			for (; i3 < two_deck; i3++)
				set_ship(two, player);
						
			for (; i4 < one_deck; i4++)
				set_ship(one, player);
			break;
		} catch (Exception& e) {
			if (player->get_flag_view()) gameview->print_exception(e.what());
		}
	}
}

bool Arbitr::check_move(shared_ptr<Player> cur_player) {
	int opponent_player = (current_player + 1) % 2;
	short X, Y;
	pair<int, int> pair;
	while (true) {
		if (cur_player->get_flag_view()) gameview->print_coord_for_strike(cur_player->get_name());
		try {
			pair = cur_player->generate_strike();
			X = pair.first;	// ��������� ��������� ����� �������
			Y = pair.second;
			check_coord_strike(X, Y);
			if (player_base[current_player]->get_val_map_strike(X, Y) == 1 || player_base[current_player]->get_val_map_strike(X, Y) == 2) throw ReStrikeException();
			player_base[current_player]->set_val_map_strike(X, Y, 1);	// � �������� ������ �� ������ � ������� ������ 1
			if (player_base[opponent_player]->impact_on_ships(X, Y)) {	// ���� ����� � �������
				if (player_base[opponent_player]->check_ship(X, Y)) {	// ��������� �� �������
					// TODO: ������� get_ship(X, Y)
					cur_player->destroy_ship(X, Y);
					Ship ship = player_base[opponent_player]->get_ship(X, Y);
					player_base[current_player]->set_ship_on_map_strike(ship);
					player_base[current_player]->add_scores();
					if (player_base[opponent_player]->get_count_ships() == 0) {
						player_base[opponent_player]->lost(); // ���� � ��������� �� �������� ��������, �� ������������� ���� ���������
						cur_player->new_strategy();
					}
				}
				else
					cur_player->deck_shot_down(X, Y);
				return true;
				// TODO: impact, check_move
			}
			if (cur_player->get_flag_view()) gameview->print_didnt_hit();
			break;
		}
		catch (Exception& e) {
			if (cur_player->get_flag_view()) gameview->print_exception(e.what());
		}
	}
	return false;
}
