#pragma once
#include <exception>

using namespace std;

class Exception : public exception {
	public:
		virtual const char* what() const throw() = 0;
};

class ValException : public Exception {
	public:
		virtual const char* what() const throw() = 0;
};

class StrException : public ValException {
	public:
		const char* what() const throw() {
			return "Error! A lot of symbols.";
		}
};

class DirException : public ValException {
public:
	const char* what() const throw() {
		return "Error! Incorrect value of direction.";
	}
};

class CoordinatesException : public ValException {
public:
	const char* what() const throw() {
		return "Error! Incorrect coordinates of ship.";
	}
};

class StrikeException : public Exception {
	public:
		virtual const char* what() const throw() = 0;
};

class ReStrikeException : public StrikeException {
public:
	const char* what() const throw() {
		return "Error! Wrong strike, you shot that cell.";
	}
};
