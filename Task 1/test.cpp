#include "pch.h"
#include "../Task_1/tritset.cpp"

TEST(Test_methods, shrink) {
	TritSet a(10);
	a[5] = True;
	a.shrink();

  EXPECT_EQ(a.capacity(), 6);
  EXPECT_TRUE(true);
}

TEST(Test_methods, cardinality) {
	TritSet a(10);
	a[5] = True;
	a[9] = True;

	EXPECT_EQ(a.cardinality(True), 2);
	EXPECT_TRUE(true);
}

TEST(Test_methods, trim) {
	TritSet a(10);
	a[5] = True;
	a[6] = True;
	a[7] = True;

	a.trim(7);

	EXPECT_EQ(a.capacity(), 8);
	EXPECT_TRUE(true);
}

TEST(Test_methods, capacity) {
	TritSet a(1001);
	a[5] = True;
	a[6] = True;
	a[7] = True;
	a[1000] = False;

	EXPECT_EQ(a.capacity(), 1001);
	EXPECT_TRUE(true);
}

TEST(Test_methods, length) {
	TritSet a(1000);
	a[0] = True;
	a[1] = Unknown;
	a[4] = True;
	a[6] = False;

	EXPECT_EQ(a.length(), 6);
	EXPECT_TRUE(true);
}

TEST(Test_methods, map_cardinality) {
	TritSet a(1000);

	a[0] = True;
	a[1] = Unknown;
	a[4] = True;
	a[6] = False;

	unordered_map<Trit, int, std::hash<int>> map;
	map = a.cardinality();

	EXPECT_EQ(map[True], 2);
	EXPECT_EQ(map[False], 1);
	EXPECT_EQ(map[Unknown], 997);
	EXPECT_TRUE(true);
}

TEST(Test_operators, index) {
	TritSet a(10);

	a[1] = True;

	EXPECT_EQ((Trit)a[1], True);
	EXPECT_TRUE(true);
}

TEST(Test_operators, assignment) {
	TritSet a(10);
	TritSet b(10);

	a[1] = Unknown;
	a[2] = a[1];
	b = a;

	EXPECT_EQ((Trit)a[1], Unknown);
	EXPECT_EQ((Trit)a[2], Unknown);
	EXPECT_EQ((Trit)b[2], Unknown);
	EXPECT_TRUE(true);
}

TEST(Test_operators, conjunction) {
	TritSet a(4);
	TritSet b(4);
	TritSet c(4);

	a[1] = True;
	a[2] = False;
	a[3] = a[1] & a[2];

	b[1] = False;
	b[2] = False;
	b[3] = Unknown;

	c = a & b;

	EXPECT_EQ((Trit)a[3], False);
	EXPECT_EQ((Trit)c[1], False);
	EXPECT_EQ((Trit)c[2], False);
	EXPECT_EQ((Trit)c[3], False);
	EXPECT_TRUE(true);
}

TEST(Test_operators, disjunction) {
	TritSet a(4);
	TritSet b(4);
	TritSet c(4);

	a[1] = True;
	a[2] = False;
	a[3] = a[1] | a[2];

	b[1] = False;
	b[2] = False;
	b[3] = Unknown;

	c = a | b;

	EXPECT_EQ((Trit)a[3], True);
	EXPECT_EQ((Trit)c[1], True);
	EXPECT_EQ((Trit)c[2], False);
	EXPECT_EQ((Trit)c[3], True);
	EXPECT_TRUE(true);
}

TEST(Test_operators, denial) {
	TritSet a(4);
	TritSet b(4);
	TritSet c(4);

	a[1] = True;
	a[2] = False;
	a[3] = ~a[1];


	c = ~a;

	EXPECT_EQ((Trit)a[3], False);
	EXPECT_EQ((Trit)c[1], False);
	EXPECT_EQ((Trit)c[2], True);
	EXPECT_EQ((Trit)c[3], True);
	EXPECT_TRUE(true);
}

TEST(Test_operators, equal) {
	TritSet a(10);
	a[1] = True;
	if (a[1] == True) a[2] = False;

	EXPECT_EQ((Trit)a[2], False);
	EXPECT_TRUE(true);
}

TEST(Test_operators, unequal) {
	TritSet a(10);
	a[1] = False;
	if (a[1] != True) a[2] = False;

	EXPECT_EQ((Trit)a[2], False);
	EXPECT_TRUE(true);
}

TEST(Test_operators, reduction) {
	TritSet a(10);
	a[2] = False;

	EXPECT_EQ((Trit)a[2], False);
	EXPECT_TRUE(true);
}