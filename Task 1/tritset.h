#include <unordered_map>
using namespace std;
using uint = unsigned int;

enum Trit { False, Unknown, True };

class TritSet {
	private:
		uint* array_of_trit;		// указатель на массив, где хранятся триты
		int count_of_trits;			// количество тритов
		
		class TritLink {	// Класс ссылки на трит
			private:
				TritSet &trit_set;
				int index;
			public:
				TritLink(TritSet &, const int);								// Конструткор класса TritLink
				TritSet::TritLink& operator = (const Trit);					// оператор присваивания
				TritSet::TritLink& operator = (const TritSet::TritLink &);	// оператор присваивания
				Trit operator & (const TritSet::TritLink &);				// оператор конъюнкции
				Trit operator | (const TritSet::TritLink &);				// оператор дизъюнкции
				Trit operator ~ ();											// оператор отрицания
				bool operator == (const Trit);								// оператор равенства
				bool operator != (const Trit);								// оператор неравенства
				operator Trit();											// оператор приведения типа к Trit
				uint get_mask_of_trit(Trit, int);
				void copy_array(uint*, int, int, int);
				Trit get_trit(TritSet &, int);
			
		};
	public:
		TritSet(size_t);					// Конструктор класса Tritset
		TritSet(const TritSet &);			// Конструктор копирования
		size_t capacity() const;			// Размерность контейнера
		~TritSet();							// Деструктор класса Tritset 
		
		TritSet::TritLink operator [] (const int);						// оператор []
		TritSet& operator = (const TritSet &);							// оператор присваивания
		friend TritSet operator & (TritSet &, TritSet &);				// оператор конъюнкции
		friend TritSet operator | (TritSet &, TritSet &);				// оператор дизъюнкции
		friend TritSet operator ~ (TritSet &);							// оператор отрицания
		size_t length();										
		void shrink();
		size_t cardinality(Trit);
		Trit get_trit(uint);
		unordered_map< Trit, int, std::hash<i