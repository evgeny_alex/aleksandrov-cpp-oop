#include "tritset.h"
#define size_trit 2
#define trit_in_uint 8*sizeof(uint)/2
#define size_byte 8

using namespace std;

/* Все операторы */

TritSet::TritLink TritSet::operator [] (int n) { // оператор индексации
	TritLink tritlink(*this, n);
	return tritlink;
}

uint TritSet::TritLink::get_mask_of_trit(Trit trit, int index_in_uint) {
	uint mask_of_trit;
	switch (trit) { // вставляем трит
	case True:
		mask_of_trit = 0x03 << ((trit_in_uint - 1 - index_in_uint) * 2);
		break;
	case False:
		mask_of_trit = 0x02 << ((trit_in_uint - 1 - index_in_uint) * 2);
		break;
	case Unknown:
		mask_of_trit = 0x00 << ((trit_in_uint - 1 - index_in_uint) * 2);
		break;
	}
	return mask_of_trit;
}

TritSet::TritLink& TritSet::TritLink::operator = (const Trit trit) { // оператор присваивания
	int len = this->trit_set.count_of_trits * size_trit / size_byte / sizeof(uint) + 1;
	if ((this->index > (int)((len)*trit_in_uint)) && (trit == Unknown)) return *this; // чтобы новая память не выделялась
	if ((this->index > (int)((len)*trit_in_uint)) && (trit != Unknown)) // проверка на выделение новой память
		copy_array(this->trit_set.array_of_trit, len, this->trit_set.count_of_trits, this->index);
	if(this->index >= this->trit_set.count_of_trits) this->trit_set.count_of_trits = this->index + 1;
		
	int index_in_uint = (this->index)% (trit_in_uint);
	uint mask1 = ~uint(0) << ((trit_in_uint - 1 - index_in_uint) * 2 + 2);
	uint mask2 = ~uint(0) >> (trit_in_uint * 2 - ((trit_in_uint - 1 - index_in_uint) * 2));
	uint mask = mask1 | mask2; // создали маску
	
	this->trit_set.array_of_trit[(this->index) / (trit_in_uint)] &= mask;
	this->trit_set.array_of_trit[(this->index) / (trit_in_uint)] |= get_mask_of_trit(trit, index_in_uint);

	return *this;
}

TritSet::TritLink& TritSet::TritLink::operator = (const TritSet::TritLink &trit_link) { // оператор присваивания для trit_link
	int index_in_uint = (this->index) % (trit_in_uint);
	uint mask1 = ~uint(0) << ((trit_in_uint - 1 - index_in_uint) * 2 + 2);
	uint mask2 = ~uint(0) >> (trit_in_uint * 2 - ((trit_in_uint - 1 - index_in_uint) * 2));
	uint mask = mask1 | mask2; // создали маску

	Trit trit = get_trit(trit_link.trit_set, trit_link.index);

	this->trit_set.array_of_trit[(this->index) / (trit_in_uint)] &= mask;
	this->trit_set.array_of_trit[(this->index) / (trit_in_uint)] |= get_mask_of_trit(trit, index_in_uint);;
	return *this;
}

Trit TritSet::TritLink::operator & (const TritSet::TritLink &trit_link) { // оператор & для отдельных элементов
	Trit trit_1 = TritSet::TritLink::get_trit(this->trit_set, this->index);
	Trit trit_2 = TritSet::TritLink::get_trit(trit_link.trit_set, trit_link.index);
	if ((trit_1 == False) || (trit_2 == False)) return False;
	if ((trit_1 == Unknown) || (trit_2 == Unknown)) return Unknown;
	return True;
}

Trit TritSet::TritLink::operator | (const TritSet::TritLink &trit_link) { // оператор | для отдельных элементов
	Trit trit_1 = TritSet::TritLink::get_trit(this->trit_set, this->index);
	Trit trit_2 = TritSet::TritLink::get_trit(trit_link.trit_set, trit_link.index);
	if ((trit_1 == True) || (trit_2 == True)) return True;
	if ((trit_1 == Unknown) || (trit_2 == Unknown)) return Unknown;
	return False;
}

Trit TritSet::TritLink::operator ~ () { // оператор ~ для отдельных элементов
	Trit trit = TritSet::TritLink::get_trit(this->trit_set, this->index);
	if (trit == True) return False;
	if (trit == Unknown) return Unknown;
	return True;
}

bool TritSet::TritLink::operator == (const Trit trit_2) {
	Trit trit_1 = TritSet::TritLink::get_trit(this->trit_set, this->index);
	return (trit_1==trit_2?true:false);
}

bool TritSet::TritLink::operator != (const Trit trit) {
	return !(*this == trit);
}

TritSet::TritLink::operator Trit() {
	return TritSet::TritLink::get_trit(this->trit_set, this->index);
}

TritSet& TritSet::operator = (const TritSet &trit_set) { // оператор присваивания (всего контейнера) 
	int len = ((this->count_of_trits < trit_set.count_of_trits)? *this : trit_set).capacity();
	int len_of_uint = len * size_trit / size_byte / sizeof(uint) + 1; // не длина массива, а количество байтов
	for (int i = 0; i < len_of_uint; i++) 
		this->array_of_trit[i] = trit_set.array_of_trit[i];
	return *this;
}

TritSet operator & (TritSet &trit_set_1, TritSet &trit_set_2) { // оператор конъюнкции
	int l = (trit_set_1.count_of_trits>trit_set_2.count_of_trits?trit_set_1.count_of_trits:trit_set_2.count_of_trits);
	TritSet a(l);
	for (int i = 0; i < l; i++)
		a[i] = trit_set_1[i] & trit_set_2[i];
	return a;
}

TritSet operator | (TritSet &trit_set_1, TritSet &trit_set_2) { // оператор дизъюнкции
	int l = (trit_set_1.count_of_trits>trit_set_2.count_of_trits ? trit_set_1.count_of_trits : trit_set_2.count_of_trits);
	TritSet set(l);
	for (int i = 0; i < l; i++)
		set[i] = trit_set_1[i] | trit_set_2[i];
	return set;
}

TritSet operator ~ (TritSet &trit_set) { // оператор отрицания
	int l = trit_set.count_of_trits;
	TritSet a(l);
	for (int i = 0; i < l; i++)
		a[i] = ~trit_set[i];
	return a;
}

/* Все методы классов */ 

TritSet::TritSet(size_t count_of_trits) { // конструктор
	TritSet::count_of_trits = count_of_trits; // кол-во тритов
	int len = TritSet::count_of_trits * size_trit / size_byte / sizeof(uint) + 1;
	TritSet::array_of_trit = new uint[len];
	for (int i = 0; i < len; i++) // весь контейнер заполнен 00, т.е. Unknown
		array_of_trit[i] = uint(0);
}

TritSet::TritSet(const TritSet &trit_set) { // конструктор копирования
	TritSet::count_of_trits = trit_set.count_of_trits;
	int len = TritSet::count_of_trits * size_trit / size_byte / sizeof(uint) + 1;
	TritSet::array_of_trit = new uint[len];
	for (int i = 0; i < len; i++)
		array_of_trit[i] = trit_set.array_of_trit[i];
}

TritSet::~TritSet() { // деструктор
	delete TritSet::array_of_trit;
}

Trit TritSet::TritLink::get_trit(TritSet &trit_set, int index) { // возвращаем трит по индексу
	int index_in_uint = (index) % (trit_in_uint);
	uint byte = trit_set.array_of_trit[index / (trit_in_uint)];
	uint mask = (byte >> (2 * (trit_in_uint)-(index_in_uint + 1) * 2)) & 0x03;
	switch (mask) {
	case 0x02:
		return False;
		break;
	case 0x03:
		return True;
		break;
	}
	return Unknown;
}

void TritSet::TritLink::copy_array(uint* array_of_trit, int length_of_array, int count_of_trits, int index) {
	int new_length_array = index * size_trit / size_byte / sizeof(uint) + 1; // новая длина
	uint* new_array_of_trit = new uint[new_length_array]; // новый массив тритов
	for (int i = 0; i < new_length_array; i++) // обнулили весь массив
		new_array_of_trit[i] = uint(0);
	for (int i = 0; i < length_of_array; i++) { // копируем элементы
		new_array_of_trit[i] = array_of_trit[i];
	}
	this->trit_set.array_of_trit = new_array_of_trit;
	this->trit_set.count_of_trits = count_of_trits;
}

TritSet::TritLink::TritLink(TritSet& trit_set, const int index) : trit_set(trit_set), index(index) {} // конструктор Tritlink

size_t TritSet::capacity() const { // возвращает кол-во тритов
	return this->count_of_trits;
}

size_t TritSet::length() { // возвращает индекс последнего не Unknown трита 
	int index = 0, index_in_uint;
	uint byte, mask;
	for (int i = 0; i < this->count_of_trits; i++) {
		index_in_uint = (i) % (trit_in_uint);
		byte = this->array_of_trit[i / (trit_in_uint)];
		mask = (byte >> (2 * (trit_in_uint)-(index_in_uint + 1) * 2)) & 0x03;
		if (mask != 0) index = i;
	}
	return index;
}

void TritSet::create_new_tritset() { // создаем новый массив для tritset
	int new_length_array = this->count_of_trits * size_trit / size_byte / sizeof(uint) + 1; // новая длина
	uint* new_array_of_trits = new uint[new_length_array]; // новый массив тритов
	for (int i = 0; i < new_length_array; i++) // обнулили весь массив
		new_array_of_trits[i] = uint(0);
	for (int i = 0; i < new_length_array; i++)  // копируем элементы
		new_array_of_trits[i] = array_of_trit[i];
	delete this->array_of_trit;

	this->array_of_trit = new_array_of_trits;
}

void TritSet::shrink() { // очищаем память после последнего не Unknown трита
	if (this->capacity() == 0) return;
	if (this->capacity() - 1 > (this->length() + 1) / (trit_in_uint * 2)) {
		this->count_of_trits = this->length() + 1;
		create_new_tritset();
	}
}

size_t TritSet::cardinality(Trit trit) { // возвращает количество тритов по значению
	int index = 0, index_in_uint, k = 0;
	uint byte, mask;
	for (int i = 0; i < this->count_of_trits; i++) {
		index_in_uint = (i) % (trit_in_uint);
		byte = this->array_of_trit[i / (trit_in_uint)];
		mask = (byte >> (2 * (trit_in_uint)-(index_in_uint + 1) * 2)) & 0x03;
		if (TritSet::get_trit(mask) == trit) k++;
	}
	return k;
}

Trit TritSet::get_trit(uint mask) { // возвращает трит
	switch (mask) {
	case 0x03:
		return True;
		break;
	case 0x02:
		return False;
		break;
	}
	return Unknown;
}

unordered_map<Trit, int, std::hash<int>> TritSet::cardinality() { // map, где хранятся триты
	unordered_map<Trit, int, std::hash<int>> map;
	int index = 0, index_in_uint;
	uint byte, mask;
	Trit trit;
	for (int i = 0; i < this->count_of_trits; i++) {
		index_in_uint = (i) % (trit_in_uint);
		byte = this->array_of_trit[i / (trit_in_uint)];
		mask = (byte >> (2 * (trit_in_uint)-(index_in_uint + 1) * 2)) & 0x03;
		trit = TritSet::get_trit(mask);
		switch (trit) { // проверяем куда добавить трит
		case True:
			map[trit]++;
			break;
		case False:
			map[trit]++;
			break;
		case Unknown:
			map[trit]++;
			break;
		}
	}
	return map;
}

void TritSet::trim(size_t last_index) { // забываем все содержимое от last_index и дальше
	this->count_of_trits = last_index + 1;
	create_new_tritset();
}