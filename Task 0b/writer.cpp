#include <fstream>
#include <string>
#include "writer.h"
#include "reader.h"
#define HUNDRED 100

using namespace std;
// Конструктор класса Writer

Writer::Writer(string file_str, const Reader &r){
	ofstream file_out(file_str);
	Writer::write_into_file(r.get_multimap(), file_out, r.get_count_words());
}

/*write_into_file - функция записывает в файл таблицу из multimap.*/

void Writer::write_into_file(const multimap<int, string> &multimap, ofstream &file_out, const int COUNT_WORDS) {
	string str;
	int percent; // проценты

	for (auto it = multimap.crbegin(); it != multimap.crend(); it++) {
		percent = it->first * HUNDRED / COUNT_WORDS;
		file_out << it->second << "," << it->first << "," << percent << "%" << "\n";
	}
}