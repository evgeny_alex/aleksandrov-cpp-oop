#pragma once
#include <string>
#include "reader.h"
#include <map>

using namespace std;

class Writer {
public:
	Writer(string, const Reader &);
private:
	void Writer::write_into_file(const multimap<int, string> &, ofstream &, const int);
};